package com.fatunicorn.euler0002;

/**
 * Created by Artem Chernyshov on 07.05.2017.
 *
 * finds the sum of the even-valued Fibonacci-values starting with 1 and 2 and not exceeding four millions
 */
public class Euler0002 {

    private final static Integer LIMIT = 4000000;

    public static void main(String[] args) {
        Integer result = compute(1, 1, 0);
        System.out.println("Result: " + result);
    }

    private static Integer compute(Integer first, Integer second, Integer sum) {
        Integer next = first + second;
        if(next >= LIMIT) {
            return sum;
        }

        if(next % 2 == 0) {
            sum += next;
        }
        return compute(second, next, sum);
    }
}
